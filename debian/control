Source: python-wither
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Mike Gabriel <sunweaver@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 python3,
 python3-pytest,
 python3-setuptools,
 dh-sequence-python3,
Standards-Version: 4.6.1
Homepage: https://pypi.python.org/pypi/wither
Vcs-Git: https://salsa.debian.org/python-team/packages/python-wither.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-wither

Package: python3-wither
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
 python3-lxml,
Description: XML/HTML Generation DSL (Python 3)
 Wither is a library designed to make XML generation under Python as
 simple and as nicely formatted as Python code.
 .
 Wither is implemented as a thin stateless wrapper around etree.Element
 objects and works by making use of the ‘with’ keyword in Python to build
 a nested tree of etree objects that can be processed with standard
 tools/techniques
 .
 by using Python as a DSL you can automatically ensure that all tags are
 properly closed and also execute arbitrary Python code to build things
 such as lists or to embed widgets
 .
 This package provides the Python 3 version of Wither.
